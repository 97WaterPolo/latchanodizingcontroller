#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//PINS
#define ONE_WIRE_BUS 4
#define SETPOINT_UP 12
#define SETPOINT_DOWN 14
#define CIRCULATION_PUMP_PIN 26
#define FAN_PIN 25
#define COMPRESSOR_PIN 33

//Time counts
#define LCD_DISPLAY_MILLIS 3000
#define TEMPERATURE_READ_MILLIS 1000
#define PUMP_EXECUTION_MILLIS 5000
#define BUTTON_PRESS_DELAY 500
#define CIRCULATION_PUMP_START_DELAY 20000
#define AC_COMPRESSOR_START_DELAY 60000
#define COMPRESSOR_FAN_OFFSET 15000
#define OUTPUT_START_TIME 15000

//Temp variations
#define TEMPERATURE_COUNT_AVG 3
#define COOLANT_SETPOINT_MIN 40.0
#define COOLANT_SETPOINT_MAX 45.0
#define TEMP_VARIATION 2





/* Prototypes */
void executeAcidBathLogic();
void executeCoolantLogic();
void readTemperatureSensors();
void printOutLCD();
double retrieveCoolantTemperature();
double retrieveAcidBathTemperature();


/* Variable Declerations */
byte heart[8] = {0b00000,0b01010,0b11111,0b11111,0b01110,0b00100,0b00000,0b00000};
DeviceAddress sensorCoolant = {0x28, 0x1, 0x29, 0xF9, 0x63, 0x20, 0x01, 0x1E};
DeviceAddress sensorAcid = {0x28, 0x9A, 0xA4, 0x78, 0x62, 0x20, 0x01, 0x8D};
LiquidCrystal_I2C lcd(0x27, 16, 2);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
bool printTemp = false;
unsigned long currentMillis = 0;
unsigned long prevLCDDisplayMillis = 0, prevTempReadMillis = 0, prevPumpExecutionMillis = 0;
unsigned long lastBtnPressUp = 0, lastBtnPressDown = 0;
unsigned long circulationPumpRestartTime = 0, airConditionerUnitRestartTime = 0;
unsigned long compressorTurnOnTime = 0, fanTurnOffTime = 0;
double setpointTemperature = 65.0;
double acidBathTemps[TEMPERATURE_COUNT_AVG], coolantTemps[TEMPERATURE_COUNT_AVG];
int tempsIndex = 0;

void setup(){
    Serial.begin(115200);
    lcd.init();
    lcd.createChar(0, heart);
    lcd.backlight();
    sensors.begin();

    pinMode(CIRCULATION_PUMP_PIN, OUTPUT);
    pinMode(COMPRESSOR_PIN, OUTPUT);
    pinMode(FAN_PIN, OUTPUT);
}

void loop(){
    currentMillis = millis();

    /* Read the Temperature Sensors */
    if ((unsigned long) (currentMillis - prevTempReadMillis) >= TEMPERATURE_READ_MILLIS) {
        prevTempReadMillis += TEMPERATURE_READ_MILLIS;
        readTemperatureSensors();
    }

    /* Execute Pump and output logic Logic */
    if ((unsigned long) (currentMillis - prevPumpExecutionMillis) >= PUMP_EXECUTION_MILLIS) {
        if (currentMillis > OUTPUT_START_TIME){ //We want to do no outputs for first 15 seconds
            prevPumpExecutionMillis += PUMP_EXECUTION_MILLIS;
            executeAcidBathLogic();
            executeCoolantLogic();
        }
    }

    /* Print out the LCD Display */
    if ((unsigned long) (currentMillis - prevLCDDisplayMillis) >= LCD_DISPLAY_MILLIS) {
        prevLCDDisplayMillis += LCD_DISPLAY_MILLIS;
        printOutLCD();
    }

    if (digitalRead(SETPOINT_UP) == HIGH
        && (unsigned long) (currentMillis - lastBtnPressUp) >= BUTTON_PRESS_DELAY){
        lastBtnPressUp = currentMillis;
        setpointTemperature++;
        printOutLCD();
    }

    if (digitalRead(SETPOINT_DOWN) == HIGH
        && (unsigned long) (currentMillis - lastBtnPressDown) >= BUTTON_PRESS_DELAY){
        lastBtnPressDown = currentMillis;
        setpointTemperature--;
        printOutLCD();
    }

}
void executeAcidBathLogic(){
    if (millis() < circulationPumpRestartTime) //Cooldown before attempting to start pump again
        return;
    int pinStatus = digitalRead(CIRCULATION_PUMP_PIN);
    double acidBathTemp = retrieveAcidBathTemperature();
    if (pinStatus == HIGH){
        //If the circulation pump is on, and it cools below our setpoint, turn it off
        if (acidBathTemp < (setpointTemperature-TEMP_VARIATION)){
            digitalWrite(CIRCULATION_PUMP_PIN, LOW);
            circulationPumpRestartTime = millis() + CIRCULATION_PUMP_START_DELAY;
            Serial.println("Turning off Acid Bath Pump " + String(acidBathTemp));
        }
    }else{
        //If the circulation pump is not on, and acidBathTemp is higher than setpoint
        if (acidBathTemp > (setpointTemperature+TEMP_VARIATION)){
            Serial.println("Turning on Acid Bath Pump " + String(acidBathTemp));
            digitalWrite(CIRCULATION_PUMP_PIN, HIGH);
        }
    }
}

void executeCoolantLogic(){
    if (millis() < airConditionerUnitRestartTime) //Cooldown before attempting to start ac again. 1min
        return;
    double coolantTemp = retrieveCoolantTemperature(); //Get temperature of coolant
    int fanStatus = digitalRead(FAN_PIN); //Get status of pin
    int compressorStatus = digitalRead(COMPRESSOR_PIN); //Get status of pin

    //Step 1 - Fan: ON   Compressor: ON  - If cooltemp is below setpoint, turn off compressor. Wait 15sec turn off fan
    //Step 2 - Fan: ON   Compressor: OFF - Fan is on for 15sec, turn on compressor
    //Step 3 - Fan: OFF  Compressor: ON  - SHOULD NEVER OCCUR WHERE COMPRESSOR ON AND FAN IS OFF
    //Step 4 - Fan: OFF  Compressor: OFF - If to hot, turn on the fan, wait 15 second (next is step 2)
    if (fanStatus == HIGH && compressorStatus == HIGH){
        if (coolantTemp <= COOLANT_SETPOINT_MIN){
            Serial.println("Turning off the compressor, wait delay for fan");
            digitalWrite(COMPRESSOR_PIN, LOW); //TURN OFF Compressor
            fanTurnOffTime = millis() + COMPRESSOR_FAN_OFFSET; //leave fan off for 15sec
            compressorTurnOnTime = 0;
        }//ELSE leave it on to cool the coolant
    }else if (fanStatus == HIGH && compressorStatus == LOW){
        if (compressorTurnOnTime != 0){ //Startup
            if (millis() >= compressorTurnOnTime){
                Serial.println("Turn on the compressor");
                digitalWrite(COMPRESSOR_PIN, HIGH); //Turn on compressor
            }//ELSE keep waiting for cooldown to start compressor
        }
        if (fanTurnOffTime != 0){ //Shutdown
            if (millis() >= fanTurnOffTime){
                Serial.println("Turn off fan, system cooldown delay");
                digitalWrite(FAN_PIN, LOW); //Turn off the fan
                airConditionerUnitRestartTime = millis() + AC_COMPRESSOR_START_DELAY; //Wait 60 sec before starting again
            }//ELSE keep waiting for cooldown to turn off the fan
        }
    }else if (fanStatus == LOW && compressorStatus == HIGH){
        //Do nothing... this shouldn't occur.
    }else{ //Both are low and low
        if (coolantTemp >= COOLANT_SETPOINT_MAX){
            Serial.println("Turn on fan, wait delay for compressor");
            digitalWrite(FAN_PIN, HIGH);
            compressorTurnOnTime = millis() + COMPRESSOR_FAN_OFFSET;
            fanTurnOffTime = 0;
        }//Else it is between the ranges so do nothing
    }
}

void readTemperatureSensors(){
    sensors.requestTemperatures(); //Get all the sensor info
    double acidBathReading = sensors.getTempF(sensorAcid);
    acidBathTemps[tempsIndex] = acidBathReading;
    double coolantReading = sensors.getTempF(sensorCoolant);;
    coolantTemps[tempsIndex] = coolantReading;

    tempsIndex++; //Increase the current index for temps
    if (tempsIndex == TEMPERATURE_COUNT_AVG)
        tempsIndex = 0; //If we exceed max avg count, reset

}

void printOutLCD(){
    Serial.println("Print out LCD " + String(currentMillis));
    String acidBathTemp = String("A:") + retrieveAcidBathTemperature();
    String coolantTemp = String("C:") + retrieveCoolantTemperature();
    String setpoint = "S:" + String(setpointTemperature);
    printTemp = !printTemp;

    Serial.println("Acid Bath Temp  " + acidBathTemp);
    Serial.println("Coolant   Temp  " + coolantTemp);
    Serial.println("Setpoint Value  " + setpoint);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(acidBathTemp);
    lcd.setCursor(8,0);
    lcd.print(coolantTemp);
    lcd.setCursor(0,1);
    lcd.print(setpoint);

    lcd.setCursor(10,1);
    lcd.print(digitalRead(CIRCULATION_PUMP_PIN));
    lcd.setCursor(12,1);
    lcd.print(digitalRead(FAN_PIN));
    lcd.setCursor(13,1);
    lcd.print(digitalRead(COMPRESSOR_PIN));

    lcd.setCursor(15,1);
    if (printTemp)
        lcd.write((byte)0);
}

double retrieveAcidBathTemperature(){
    double x = 0;
    for (int i = 0; i < TEMPERATURE_COUNT_AVG; i++){
        if (acidBathTemps[i] == 0)
            continue;
        x+= acidBathTemps[i];
    }
    return x/TEMPERATURE_COUNT_AVG;
}

double retrieveCoolantTemperature(){
    double x = 0;
    for (int i = 0; i < TEMPERATURE_COUNT_AVG; i++){
        if (coolantTemps[i] == 0)
            continue;
        x+= coolantTemps[i];
    }
    return x/TEMPERATURE_COUNT_AVG;
}

